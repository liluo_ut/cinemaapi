# createtime:2021/3/26 14:01
# user:luoli
# project:SHClass35
import time

from cinema_test_api.action.do_moviceQuery2 import DomoviequertGUI
import unittest
from common.screen import Screendriver


class TestmovicequertGUI(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.case = DomoviequertGUI()

    def test_case1(self):
        resp = self.case.moviequeryGUI_case()
        try:
            self.assertEqual("天气之子", resp.text)
        except Exception:
            Screendriver().screen()
            raise Exception
        finally:
            self.case.do.page4().click()


if __name__ == '__main__':
    unittest.main()
