import time

import requests
from ddt import ddt, data, unpack

from action.do_login import DoLogin
from common.ReadDb import ReadDb

from common.ReadFile import ReadFile
from common.read_csv import readcsv
import unittest
from cinema_test_api.common.GetmathodPath import get_mathodPath
from cinema_test_api.common.Getpath import get_path


@ddt
class TestLogin(unittest.TestCase):
    path = get_path() + '/testdata/login.csv'

    @classmethod
    def setUpClass(cls) -> None:
        # self.db = ReadDb()
        cls.test = DoLogin()
        cls.db = ReadDb()

    @data(*readcsv(path))
    @unpack
    def test_login_case1(self, caseid, title, account, password, expect):
        get_mathodPath(self, caseid, title)

        resp = self.test.dologin(account, password)

        test_time = time.strftime('%Y-%m-%d %H:%M:%S')
        test_type = "api测试"
        test_module = '登录'
        self.assertEquals(resp.text, expect)


if __name__ == '__main__':
    unittest.main()
