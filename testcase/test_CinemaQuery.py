# createtime:2021/3/24 16:23
# user:luoli
# project:pycharmprojects
import os

from cinema_test_api.action.do_CinemaQuery import DoCinemaQuery
import unittest
from cinema_test_api.common.GetmathodPath import get_mathodPath
from ddt import ddt, data, unpack
from cinema_test_api.common.read_csv import readcsv


@ddt()
class TestCinemaQuery(unittest.TestCase):
    path = os.path.dirname(os.path.dirname(__file__)) + '/testdata/cinemaQuery.csv'

    @classmethod
    def setUpClass(cls) -> None:
        cls.case = DoCinemaQuery()
        # cls.host = "http://192.168.0.111:8080"
        # cls.cinemaName = "太平洋影城(杭州下沙店)"

    def setUp(self) -> None:
        pass

    @data(*readcsv(path))
    @unpack
    def test_case(self, caseid, casetitle, host, provice, city, brand, hallType, area, page, expect):
        get_mathodPath(self, caseid, casetitle)
        resp = self.case.test_case1(host, provice, city, brand, hallType, area, page)
        self.assertEqual(resp.json().get("list")[0].get("cinemaName"), expect)

    def tearDown(self) -> None:
        pass

    @classmethod
    def tearDownClass(cls) -> None:
        pass


if __name__ == '__main__':
    unittest.main()
