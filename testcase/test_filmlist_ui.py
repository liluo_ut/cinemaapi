
from action.do_filmlist import DoFilmList
from page.filmlist import FilmList


class TestFilmList:
    def __init__(self):
        self.dodetails = DoFilmList()
        self.details = FilmList()

    def testfilmlist(self):
        self.dodetails.dofilmlist()
        if self.details.filmlist_1().text == "榜单规则：将昨日国内热映的影片，按照评分从高到低排列取前10名，每天上午10点更新。相关数据来源于“猫眼专业版”及“猫眼电影库”。":
            print('进入榜单界面-测试通过')
        else:
            print('没有进入榜单界面-测试失败')

    def testfilmlist_2(self):
        self.dodetails.dofilmlist()
        if self.details.filmlist_2().text == '诛仙1':
            print('进入热映口碑榜-测试通过')
        else:
            print('没有进入热映口碑榜-测试失败')

    def testfilmlist_3(self):

        self.dodetails.dofilmlist2()
        if self.details.filmlist_3().text == '特惠购票':
            print('进入电影详情界面-测试通过')
        else:
            print('没有进入电影详情界面-测试通过')


if __name__ == '__main__':
    a = TestFilmList()
    a.testfilmlist()
    a.testfilmlist_2()
    a.testfilmlist_3()