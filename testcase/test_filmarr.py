from action.do_filmarr import DoFilmArr
from common.ReadFile import ReadFile
from common.ReadDb import ReadDb
import time


class Test_FilmArr:
    def __init__(self):
        self.farr=DoFilmArr()
        self.db = ReadDb()


    @ReadFile('../testdata/filmarr.csv')
    def test_farr_case1(self,caseid,title,username, cinemaId, fareMoney, hallId, startTime, movie, movieId, timer, expect):
        resp = self.farr.FilmArr(username, cinemaId, fareMoney, hallId, startTime, movie, movieId, timer)
        test_time = time.strftime('%Y-%m-%d %H:%M:%S')
        test_type = "api测试"
        test_module = '影院排片'
        if resp.text == expect:
            print(f'{caseid}--{title}-测试通过')
            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","success","无","{test_time}"' \
                  f');'
            self.db.insert(sql)
        else:
            print(f'{caseid}--{title}-测试失败')
            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","failed","无","{test_time}"' \
                  f');'
            self.db.insert(sql)


if __name__ == '__main__':
    bdadd=Test_FilmArr()
    bdadd.test_farr_case1()
