import os
import time
import unittest

from action.do_addhall import DoAddhall
from common.GetmathodPath import get_mathodPath
from common.ReadDb import ReadDb
from common.ReadFile import ReadFile

#  用的是atm方法
class TestAddhall(unittest.TestCase):
    path = os.path.dirname(os.path.dirname(__file__)) + "/testdata/addhall.csv"

    @classmethod
    def setUpClass(cls) -> None:
        cls.add = DoAddhall()
        cls.db = ReadDb()

    @ReadFile(path)
    def test_addhall_case(self,caseid,title,hallName,hallType,hallX,hallY,cinemaId,expect):
        resp = self.add.doaddhall_api(caseid,title,hallName,hallType,hallX,hallY,cinemaId,expect)
        test_time = time.strftime('%Y-%m-%d %H:%M:%S')
        test_type = "api测试"
        test_module = '登录'
        if resp.text == expect:
            print(f'{caseid}--{title}--测试通过')
            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","success","无","{test_time}"' \
                  f');'
            self.db.insert(sql)
        # get_mathodPath(self, caseid, title)
        else:
            print(f'{caseid}--{title}--测试失败')
            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","failed","无","{test_time}"' \
                  f');'
            self.db.insert(sql)

if __name__ == '__main__':
    unittest.main()

