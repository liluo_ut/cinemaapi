import os
import time
import unittest

from ddt import ddt, data, unpack

from action.do_addhall import DoAddhall
from common.GetmathodPath import get_mathodPath
from common.ReadDb import ReadDb
from common.ReadFile import ReadFile
from common.read_csv import readcsv



@ddt
class TestAddhall2(unittest.TestCase):
    path = os.path.dirname(os.path.dirname(__file__)) + "/testdata/addhall.csv"

    @classmethod
    def setUpClass(cls) -> None:
        cls.add = DoAddhall()
        cls.db = ReadDb()


    @data(*readcsv(path))  # *是解包
    @unpack
    def test_addhall_case(self,caseid,title,hallName,hallType,hallX,hallY,cinemaId,expect):
        get_mathodPath(self, caseid, title)
        resp = self.add.doaddhall_api(caseid,title,hallName,hallType,hallX,hallY,cinemaId,expect)
        self.assertEqual(resp.text,'success',msg='测试通过')




if __name__ == '__main__':
    unittest.main()