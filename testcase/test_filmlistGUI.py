from cinema_test_api.action.do_filmlistGUI import DoFilmListGUI
import unittest
from common.screen import Screendriver


class TestfilmlistGUI(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.case = DoFilmListGUI()

    def test_case1(self):
        resp = self.case.filmlistGUI_case()
        try:
            self.assertEqual("榜单规则：将昨日国内热映的影片，按照评分从高到低排列取前10名，每天上午10点更新。相关数据来源于“猫眼专业版”及“猫眼电影库”。", resp.text)
            print("successful")
        except Exception:
            print("failed")
            Screendriver().screen()
            raise Exception


if __name__ == '__main__':
    unittest.main()

