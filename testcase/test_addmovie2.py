import os
import unittest

from ddt import unpack, data, ddt

from action.do_addmovie import DoAddmovie
from common.GetmathodPath import get_mathodPath
from common.ReadFile import ReadFile
from common.read_csv import readcsv

@ddt
class TestAddMovie2(unittest.TestCase):
    path = os.path.dirname(os.path.dirname(__file__)) + "/testdata/addmovie.csv"

    @classmethod
    def setUpClass(cls) -> None:
        cls.addmovie = DoAddmovie()


    @data(*readcsv(path))
    @unpack
    def test_addmovie(self,caseid,title,movieName,movieEname,movieTypes,expect):
        get_mathodPath(self, caseid, title)
        resp = self.addmovie.doaddmovie_api(movieName,movieEname,movieTypes)
        # print(resp.json()["result"])
        self.assertEqual(resp.json()["result"],expect,msg='测试通过')


if __name__ == '__main__':
    unittest.main()