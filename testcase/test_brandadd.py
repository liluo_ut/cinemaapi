import time
from common.ReadDb import ReadDb
from action.do_brandadd import DoBrandadd
from common.ReadFile import ReadFile


class Test_BrandAdd:
    def __init__(self):
        self.bdadd=DoBrandadd()
        self.db = ReadDb()

    @ReadFile('../testdata/brandadd.csv')
    def test_brandadd_case1(self,caseid,title,brandName,expect):


            resp = self.bdadd.Brandadd( brandName)
            test_time = time.strftime('%Y-%m-%d %H:%M:%S')
            test_type = "api测试"
            test_module = '品牌新增'
            if resp.text == expect:
                print(f'{caseid}--{title}-测试通过')

                sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                      f'values ("{test_type}","{test_module}","{caseid}","{title}","success","无","{test_time}"' \
                      f');'
                # print(sql)
                self.db.insert(sql)
            else:
                print(f'{caseid}--{title}-测试失败')
                sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                      f'values ("{test_type}","{test_module}","{caseid}","{title}","failed","无","{test_time}"' \
                      f');'
                # print(sql)
                self.db.insert(sql)



if __name__ == '__main__':
    bdadd=Test_BrandAdd()
    bdadd.test_brandadd_case1()