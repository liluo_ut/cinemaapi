import time

from ddt import ddt, data, unpack

from common.ReadDb import ReadDb
from action.do_brandadd import DoBrandadd
from common.ReadFile import ReadFile
from common.read_csv import readcsv
import unittest
from cinema_test_api.common.GetmathodPath import get_mathodPath
from cinema_test_api.common.Getpath import get_path
@ddt
class Test_BrandAdd(unittest.TestCase):
    path=get_path()+'/testdata/brandadd.csv'
    @classmethod
    def setUpClass(cls) -> None:
        cls.bdadd=DoBrandadd()
        # cls.db = ReadDb()

    @data(*readcsv(path))
    @unpack
    def test_brandadd_case1(self,caseid,title,brandName,expect):
        print(self.path)
        get_mathodPath(self,caseid,title)
        resp = self.bdadd.Brandadd( brandName)
        test_time = time.strftime('%Y-%m-%d %H:%M:%S')
        test_type = "api测试-品牌新增"
        test_module = '品牌新增'
        self.assertEquals(resp.text,expect)
        # try:
        #     if resp.text == expect:
        #         print(f'{caseid}--{title}-测试通过')
        # expect Exception as e:
        #     raise e



if __name__ == '__main__':
    unittest.main()