from action.do_moviedetails_ui import DoMoviedetails
from common.screen import Screendriver
from page.moviedetails import MovieDetails
import unittest

class TestMoviedetails(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.dodetails = DoMoviedetails()
        cls.details = MovieDetails()
        cls.screen = Screendriver()
    
    def testmoviedetails(self):
        self.dodetails.domoviedetails()
        resp = self.details.moviedetails_4()
        try:
            self.assertEqual(resp.text,"用户评分",msg='电影详情页测试成功')
        except Exception:
            print('电影详情页测试失败')
            self.screen.screen()
            raise Exception



        # if resp.text =="用户评分":
        #     print('电影详情页测试成功')
        # else:
        #     print('电影详情页测试失败')

if __name__ == '__main__':
    unittest.main()