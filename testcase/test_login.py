import time

import requests

from action.do_login import DoLogin
from common.ReadDb import ReadDb

from common.ReadFile import ReadFile



class  TestLogin:
    def __init__(self):
        # self.db = ReadDb()
        self.test = DoLogin()
        self.db=ReadDb()

    @ReadFile('../testdata/login.csv')
    def test_login_case1(self,caseid,title,account,password,expect):

        resp = self.test.dologin(account, password)


        test_time = time.strftime('%Y-%m-%d %H:%M:%S')
        test_type = "api测试"
        test_module = '登录'
        if resp.text == expect:
            print(f'{caseid}--{title}-测试通过')

            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","success","无","{test_time}"' \
                  f');'
            # print(sql)
            self.db.insert(sql)
        else:
            print(f'{caseid}--{title}-测试失败')

            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","failed","无","{test_time}"' \
                  f');'
            # print(sql)
            self.db.insert(sql)




if __name__ == '__main__':
    testlogin=TestLogin()
    testlogin.test_login_case1()