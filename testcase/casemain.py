# createtime:2021/3/24 16:57
# user:luoli
# project:pycharmprojects

import unittest
import os

from cinema_test_api.common.DIYBeautifulReport import DIYBeautifulReport
from testcase.test_addhall2 import TestAddhall2
from testcase.test_addmovie2 import TestAddMovie2
from testcase.test_CinemaQuery import TestCinemaQuery
from testcase.test_MovieQuery import TestMovieQuery
from testcase.test_brandadd2 import Test_BrandAdd
from testcase.test_login2 import TestLogin
from testcase.test_filmarr2 import Test_FilmArr
from testcase.test_cinemarank import Test_CinemaRank


def casemain():
    suite = unittest.TestSuite()
    case1 = unittest.TestLoader().loadTestsFromTestCase(TestAddhall2)  # 添加测试用例的类名
    case2 = unittest.TestLoader().loadTestsFromTestCase(TestAddMovie2)
    case3 = unittest.TestLoader().loadTestsFromTestCase(TestCinemaQuery)
    case4 = unittest.TestLoader().loadTestsFromTestCase(TestMovieQuery)
    case5 = unittest.TestLoader().loadTestsFromTestCase(Test_BrandAdd)
    case6 = unittest.TestLoader().loadTestsFromTestCase(TestLogin)
    case7 = unittest.TestLoader().loadTestsFromTestCase(Test_FilmArr)
    case8 = unittest.TestLoader().loadTestsFromTestCase(Test_CinemaRank)

    suite.addTest(case1)
    suite.addTest(case2)
    suite.addTest(case3)
    suite.addTest(case4)
    suite.addTest(case5)
    suite.addTest(case6)
    suite.addTest(case7)
    suite.addTest(case8)

    runner = DIYBeautifulReport(suite)

    path = os.path.dirname(__file__)  # 获取当前文件夹的上层路径
    reprot_path = os.path.dirname(path)  # 获取当前文件夹的上层路径
    runner.report("自动化测试报告", 'report_test.html', report_dir=(reprot_path + "/report"))


if __name__ == '__main__':
    casemain()
