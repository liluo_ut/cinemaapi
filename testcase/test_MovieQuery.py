# createtime:2021/3/24 14:38
# user:luoli
# project:pycharmprojects
import os

from cinema_test_api.action.do_MovieQuery import DoMovieQuery
import unittest
from cinema_test_api.common.GetmathodPath import get_mathodPath
from ddt import ddt, data, unpack
from cinema_test_api.common.read_csv import readcsv


@ddt()
class TestMovieQuery(unittest.TestCase):
    path = os.path.dirname(os.path.dirname(__file__)) + '/testdata/movieQuery.csv'

    @classmethod
    def setUpClass(cls) -> None:
        cls.case = DoMovieQuery()

    @data(*readcsv(path))
    @unpack
    def test_case1(self, caseid, casetitle, host, movieName, expect):
        get_mathodPath(self, caseid, casetitle)  # 将用例ID和用例抬头写入对象的__dict__中去
        resp = self.case.test_case1(host, movieName)
        self.assertEqual(expect, str(resp.status_code), msg=f'{caseid}')


if __name__ == '__main__':
    unittest.main()
