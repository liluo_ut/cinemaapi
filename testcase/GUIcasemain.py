# createtime:2021/3/26 14:24
# user:luoli
# project:SHClass35

import unittest
import os
from BeautifulReport import BeautifulReport
from testcase.test_cinemaqueryGUI import TestcinemaquertGUI
from testcase.tets_movieQuery2GUI import TestmovicequertGUI


def GUIcasemain():
    suite = unittest.TestSuite()

    case9 = unittest.TestLoader().loadTestsFromTestCase(TestmovicequertGUI)
    case10 = unittest.TestLoader().loadTestsFromTestCase(TestcinemaquertGUI)

    suite.addTest(case9)
    suite.addTest(case10)

    runner = BeautifulReport(suite)

    path = os.path.dirname(__file__)  # 获取当前文件夹的上层路径
    reprot_path = os.path.dirname(path)  # 获取当前文件夹的上层路径
    runner.report("自动化测试报告", 'GUIreport_test.html', report_dir=(reprot_path + "/report"))


if __name__ == '__main__':
    GUIcasemain()
