import os
import time
import unittest

from action.do_addmovie import DoAddmovie
from common.ReadDb import ReadDb
from common.ReadFile import ReadFile

#  用的是atm方法
class TestAddMovie(unittest.TestCase):
    path = os.path.dirname(os.path.dirname(__file__)) + "/testdata/addmovie.csv"

    @classmethod
    def setUpClass(cls) -> None:
        cls.addmovie = DoAddmovie()
        cls.db = ReadDb()

    @ReadFile(path)
    def test_addmovie(self,caseid,title,movieName,movieEname,movieTypes,expect):
        resp = self.addmovie.doaddmovie_api(movieName,movieEname,movieTypes)
        test_time = time.strftime('%Y-%m-%d %H:%M:%S')
        test_type = "api测试"
        test_module = '登录'
        # print(resp.json()["result"])
        if resp.json()["result"] == expect:
            print(f'{caseid}--{title}--测试通过')
            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","success","无","{test_time}"' \
                  f');'
            self.db.insert(sql)
        else:
            print(f'{caseid}--{title}--测试失败')
            sql = f'insert into result (type,module,case_id,title,result,img,datetime) ' \
                  f'values ("{test_type}","{test_module}","{caseid}","{title}","failed","无","{test_time}"' \
                  f');'
            self.db.insert(sql)


if __name__ == '__main__':
    unittest.main()