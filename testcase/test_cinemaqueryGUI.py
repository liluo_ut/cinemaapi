# createtime:2021/3/25 19:44
# user:luoli
# project:SHClass35
import time

from cinema_test_api.action.do_cinemaQueryGUI import DocinemaquertGUI
import unittest
from common.screen import Screendriver


class TestcinemaquertGUI(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.case = DocinemaquertGUI()

    def test_case1(self):
        resp = self.case.moviequeryGUI_case()
        try:
            self.assertEqual("万达", resp.text)
            print("successful")
        except Exception:
            print("failed")
            Screendriver().screen()
            raise Exception
        finally:
            self.case.do.page3().click()


if __name__ == '__main__':
    unittest.main()
