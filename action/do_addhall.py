import requests
import unittest

from common.ReadFile import ReadFile
from common.GetmathodPath import get_mathodPath
import os
class DoAddhall:
    def __init__(self):
        self.url = 'http://192.168.1.49:8080/hall/addHall'

    def doaddhall_api(self,caseid,title,hallName,hallType,hallX,hallY,cinemaId,expect):
        data = {"tickets":[{"row":0,"col":0,"seatInfo":"第1排第1列"}],"hallName":hallName,"hallType":hallType,"hallX":hallX,"hallY":hallY,"cinemaId":cinemaId}

        # print(self.__dict__)
        return requests.post(url=self.url,json=data)

# if __name__ == '__main__':
#     do=DoAddhall()
#     do.doaddhall_api()
