from cinema_test_api.page.filmlistPage import FilmList


class DoFilmListGUI():
    def __init__(self):
        self.do = FilmList()


    def filmlistGUI_case(self):
        # self.do.page1().click()
        # self.do.driver.implicitly_wait(5)
        # self.do.page2().click()
        # return self.do.page4()
        self.do.filmlist_page1().click()
        return self.do.filmlist_page2()
