import requests


class DoFilmArr:
    def __init__(self):
        self.host = 'http://192.168.1.49:8080/'


    def FilmArr(self, caseid,title,username, id, fareMoney, hallId, startTime, movie, movieId, timer,expect):
        data = {'username': username,
                'cinemaId': id,
                'fareMoney': fareMoney,
                'hallId': hallId,
                'startTime': startTime,
                'movie': movie,
                'movieId': movieId,
                'time': timer
                }
        resp = requests.post(f'{self.host}/cinema/addHallMovie', data=data)
        return resp
