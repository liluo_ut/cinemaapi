import time

from page.filmlist import FilmList

class DoFilmList:
    def __init__(self):
        self.details = FilmList()

    def dofilmlist(self):
        self.details.filmlist_implement1().click()
    def dofilmlist2(self):
        self.details.filmlist_implement2().click()


if __name__ == '__main__':
    t = DoFilmList()
    t.dofilmlist()
