from common.Driver import Driver
from page.moviedetails import MovieDetails



class DoMoviedetails:
    def __init__(self):
        self.details = MovieDetails()
        self.driver = Driver.get_driver()

    def domoviedetails(self):
        self.details.moviedetails_1().click()
        self.details.moviedetails_2().click()
        self.driver.implicitly_wait(10)
        self.details.moviedetails_3().click()



if __name__ == '__main__':
    t = DoMoviedetails()
    t.domoviedetails()