import requests


class DoAddmovie:
    def __init__(self):
        self.url = 'http://192.168.1.49:8080/movie/addMovie'

    def doaddmovie_api(self,movieName,movieEname,movieTypes):
        data = {"movieName":movieName,
                "movieEname":movieEname,
                "movieTypes":movieTypes,
                "movieReleaseTime":"",
                "movieTime":"",
                "movieArea":"",
                "movieInfo":"",
                "cast0":'',
                "cast1":'',
                "cast1Role":""}

        return requests.post(self.url,data=data,allow_redirects=False)


# if __name__ == '__main__':
#     t = DoAddmovie()
#     t.doaddmovie_api()

