# createtime:2021/3/26 12:25
# user:luoli
# project:SHClass35
from cinema_test_api.page.movieQueryPage2 import moviequeryPage


class DomoviequertGUI():
    def __init__(self):
        self.do = moviequeryPage()

    def moviequeryGUI_case(self):
        self.do.page1().click()
        self.do.driver.implicitly_wait(5)
        self.do.page2().click()
        return self.do.page3()


if __name__ == '__main__':
    do = DomoviequertGUI()
    do.moviequeryGUI_case()