# createtime:2021/3/25 19:34
# user:luoli
# project:SHClass35
import time

from cinema_test_api.page.cinemaQueryPage import cinemaqueryPage


class DocinemaquertGUI():
    def __init__(self):
        self.do = cinemaqueryPage()

    def moviequeryGUI_case(self):
        self.do.page1().click()
        self.do.driver.implicitly_wait(5)
        self.do.page2().click()
        return self.do.page4()


if __name__ == '__main__':
    do = DocinemaquertGUI()
    do.moviequeryGUI_case()
