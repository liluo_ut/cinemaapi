# createtime:2021/3/25 19:27
# user:luoli
# project:SHClass35
from common.Driver import Driver
from common.GetElement import Get_Element


class cinemaqueryPage():
    def __init__(self):
        self.driver = Driver().get_driver()
        self.element = Get_Element()

    def page1(self):
        return self.element.get_element('link_text', '影院')

    def page2(self):
        return self.element.get_element('link_text', '和平影城城')

    def page3(self):
        return self.element.get_element('link_text', '首页')

    def page4(self):
        return self.element.get_element('xpath', '//*[@id="vueCinema"]//li[2]/div[1]/a')