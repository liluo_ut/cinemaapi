from common.Driver import Driver
from common.GetElement import Get_Element


class MovieDetails:
    def __init__(self):
        self.driver = Driver.get_driver()
        self.element = Get_Element()

    def moviedetails_1(self):
        return self.element.get_element('xpath',"/html/body/header/nav/ul/li[2]/a")

    def moviedetails_2(self):
        return self.element.get_element('xpath',"//a[text()='即将上映']")

    def moviedetails_3(self):
        return self.element.get_element('xpath','//*[@id="bodyMovie"]/dl/span[1]/dd/div[2]/a')#xpath','//*[@id="bodyMovie"]/dl/span[1]/dd/div[1]/a[1]

    def moviedetails_4(self):
        return self.element.get_element('xpath','//*[@id="moviedetail"]/div[3]/div[1]/p')
