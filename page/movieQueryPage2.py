# createtime:2021/3/26 12:12
# user:luoli
# project:SHClass35
from common.Driver import Driver
from common.GetElement import Get_Element


class moviequeryPage():
    def __init__(self):
        self.driver = Driver().get_driver()
        self.element = Get_Element()

    def page1(self):
        return self.element.get_element('link_text', '电影')

    def page2(self):
        return self.element.get_element('link_text', '奇幻')

    def page3(self):
        return self.element.get_element('xpath', '//*[@class="yingpian"]//span[2]//div[2]/a')

    def page4(self):
        return self.element.get_element('link_text', '首页')
