from common.Driver import Driver


class Get_Element:
    def __init__(self):
        self.driver = Driver.get_driver()

    def get_element(self, what, value):
        if what == 'id':
            return self.driver.find_element_by_id(value)
        if what == 'name':
            return self.driver.find_element_by_name(value)
        if what == 'xpath':
            return self.driver.find_element_by_xpath(value)
        if what == 'link_text':
            return self.driver.find_element_by_link_text(value)
        if what == 'tag_name':
            return self.driver.find_element_by_tag_name(value)

