import csv
from cinema_test_api.common.GetmathodPath import get_mathodPath
def ReadFile(name):  # --装饰器函数,参数--装饰器自身参数
    def wrapper(func):  # --内嵌函数，参数--被装饰函数
        def readfile(self,*args):   # --装饰器本身的函数，*args--下面的实参*line
            with open(name,encoding='utf-8') as file:
                lines = csv.reader(file)
                next(lines)
                for line in lines:
                    func(self,*line)  # 实参  ，*解包
        return readfile
    return wrapper

