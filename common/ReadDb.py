"""
1、建立连接
2、获取游标
3、执行sql
4、对于插入和更新操作还需要提交

Cursor,SSCursor : 返回列表嵌套元祖格式
DictCursor：返回列表嵌套字典格式
SSDictCursor: 返回列表嵌套字典格式
"""
import pymysql
from pymysql.cursors import SSDictCursor

class ReadDb:
    def __init__(self):
        self.conn = pymysql.connect(
            host='192.168.1.49',
            port=3306,
            user='root',
            passwd='123456',
            database='cinema',
            charset='utf8'
        )
        self.cur = self.conn.cursor(cursor=SSDictCursor)


    def query_one(self,sql):
        self.cur.execute(sql)
        return self.cur.fetchone()

    def query_all(self,sql):
        self.cur.execute(sql)
        return self.cur.fetchall()

    def insert(self,sql):
        self.cur.execute(sql)
        self.conn.commit()

    def close(self):
        self.cur.close()
        self.conn.close()





