# createtime:2021/3/26 11:27
# user:luoli
# project:SHClass35
import time
from common.Getpath import get_path
from common.Driver import Driver


class Screendriver():
    '''
    截图
    '''

    def __init__(self):
        self.driver = Driver.get_driver()

    def screen(self):
        filename = time.strftime('%Y%m%d%H%M%S', time.localtime())
        path = get_path() + f'/report/img/{filename}.png'
        self.driver.save_screenshot(path)


if __name__ == '__main__':
    s = Screendriver()
    s.screen()
