from selenium import webdriver


class Driver:
    driver = None

    @classmethod
    def get_driver(cls):
        if cls.driver is None:
            cls.driver = webdriver.Chrome()
            cls.driver.get('http://192.168.1.49:8080/business/HTML/index.html')
            cls.driver.maximize_window()
        return cls.driver



