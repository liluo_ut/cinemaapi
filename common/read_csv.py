# createtime:2021/1/18 9:48
# user:luoli
# project:untitled
import csv

# 读取CSV为字典
import os


def readcsv(filename):
    with open(filename, encoding='utf-8') as file:
        item = csv.DictReader(file)  # dictreader()返回字典
        lis = []
        for i in item:
            lis.append(i)
        return lis


if __name__ == '__main__':
    path = os.path.dirname(os.path.dirname(__file__)) + "/testdata/addhall.csv"
    print(readcsv(path))
