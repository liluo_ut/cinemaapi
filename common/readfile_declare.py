# createtime:2021/3/5 15:47
# user:luoli
# project:CLS35

class Readfiledeclare(object):
    def __init__(self, path):
        self.path = path

    def __call__(self, func):
        def readfile(func_self, *args):
            with open(self.path, encoding='utf-8') as file:
                lines = file.readlines()
            count = 0
            for line in lines:
                if count == 0:
                    count += 1
                    continue
                else:
                    list = []
                    list.extend(line.strip().split(','))
                    func(func_self, *list)

        return readfile

