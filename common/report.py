"""

生成测试报告
1、读取模板文件
2、读取数据库测试结果记录，分析和计算
3、把分析的结果替换模板文件中的模板变量
4、写入新的html文件

"""
import os
import time

from common.ReadDb import ReadDb


class Report:
    def __init__(self):
        self.data = ReadDb()

    def generate_report(self):
        # 读取模板文件
        with open('../report/template-new.html',encoding='utf-8') as file:
            template = file.read()
        # print(template)



        # 1.从数据库中获取执行[成功]的总用例数
        sql_success = "select count(*) from result where result='success';"
        result_success = self.data.query_all(sql_success)  # 返回列表嵌套字典
        pass_count = result_success[0]['count(*)']
        print(pass_count)

        # 从数据库中获取执行[失败]的总用例数
        sql_failed = "select count(*) from result where result='failed';"
        result_failed = self.data.query_all(sql_failed)
        failed_count = result_failed[0]['count(*)']
        print(failed_count)


        #计算失败率
        fail_rate_number = failed_count/(failed_count+pass_count)
        fail_rate = '%.2f%%'%(fail_rate_number *100 )
        print(fail_rate)


        sql_all = 'select * from result;'
        result_all = self.data.query_all(sql_all)   # result_all列表嵌套字典的格式
        # print(result_all)


        text = ''
        for result in result_all:
            print(result["img"])
            text += '\n<tr height="40" bgcolor="darkseagreen">'
            text += f'\n<td width="7%">{result["id"]}</td>'
            text += f'\n<td width="10%">{result["type"]}</td>'
            text += f'\n<td width="10%">{result["module"]}</td>'
            text += f'\n<td width="7%">{result["title"]}</td>'
            text += f'\n<td width="7%">{result["result"]}</td>'
            if result["img"] != '无':
                text += f'<td width="10%"><a href="{result["img"]}" traget="_blank">查看截图 </td>'
            else:

                text += f'\n<td width="10%">{result["img"]}</td>'
            text += '\n</tr>'
            # print(text)

        '''
        <tr height="40" bgcolor="darkseagreen">
		<td width="7%">记录编号</td>
		<td width="10%">测试类型</td>
		<td width="9%">所属模块</td>
		<td width="7%">用例编号</td>
		<td width="7%">测试结果</td>
		<td width="10%">现场截图</td>
	    </tr>
        '''


        # 获取测试时间
        test_date = result_all[1]['datetime']  # 随便取哪一条
        # print(test_date)                   # 2021-03-22 15:21:20
        test_date = test_date.strftime('%Y%m%d')
        # print(type(test_date))             # <class 'str'>

        # 用数据库查询出来的结果---替换模板中的变量
        template = template.replace('$pass-count',str(pass_count))
        template = template.replace('$fail-count',str(failed_count))
        template = template.replace('$fail-rate',str(fail_rate))
        template = template.replace('$test-date',test_date)
        content = template.replace('$test-result',text)
        print(text)


        # 把替换后的内容写入新的html文件，生成报告
        report_file = time.strftime('%Y%m%d%H%M%S')
        with open(F'../report/{report_file}.html',mode='w',encoding='utf-8') as file:
            file.write(content)


    def get_path(self):   # 获得路径
        p = os.path.abspath(__file__)
        path = os.path.dirname(os.path.dirname(p))
        print(path)
        report_path = os.path.join(path,'report')
        print(report_path)
        return report_path


if __name__ == '__main__':
    report = Report()
    report.generate_report()
    report.get_path()