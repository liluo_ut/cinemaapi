# createtime:2021/1/14 16:45
# user:luoli
# project:untitled
# sellsumid=278&barcode=12312312
def str_to_dict(string):
    di = {}
    for i in string.split('&'):
        a = i.split('=')
        di[a[0]] = a[1]
    return di

if __name__ == '__main__':
    print(str_to_dict('account=1&password=1&commit=登录'))